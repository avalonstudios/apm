{{--
  Title: Display Product
  Description: Displays a single procuct
  Category: ava_block_category
  Icon: admin-comments
  Keywords: product
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

if ( ! $active ) {
  return;
}

$other_classes = '';
$backImg = '';

$sectionTitle = $flds[ 'section_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $block[ 'id' ],
  'secProps'        => $flds[ 'styles' ],
  'backImg'         => ''
];

$pO = $flds[ 'product' ]; // Product Object
@endphp

@component( 'comps.blocks', $componentVars )
  @php
  $ID = $pO->ID;
  $thumb = get_the_post_thumbnail_url( $ID );
  $thumb = aq_resize( $thumb, 390 );
  $link = get_the_permalink( $ID );
  $text = wp_kses_post( substr( $pO->post_content, 0, 950 ) ) . '...';
  $button[ 'url' ]      = $link;
  $button[ 'target' ]   = '';
  $button[ 'title' ]    = 'Donate';
  @endphp
  <div class="product-grid">
    <div class="image"><img src="{{ $thumb }}" alt="{{ $pO->post_title }}"></div>
    <div class="text">{!! $text !!}</div>
    @include('comps.btns.btn')
  </div>
@endcomponent
