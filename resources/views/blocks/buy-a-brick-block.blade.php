{{--
  Title: Buy a Brick
  Description: Buy a brick donation campaign
  Category: ava_block_category
  Icon: admin-comments
  Keywords: keywords
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

if ( ! $active ) {
  return;
}

$other_classes = '';
$backImg = '';

$sectionTitle = $flds[ 'section_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => $sectionTitle,
  'blockID'         => $block[ 'id' ],
  'secProps'        => $flds[ 'styles' ],
  'backImg'         => ''
];


$less             = explode( "\n", $flds[ 'less_than_fifteen' ] );
$fifteens         = explode( "\n", $flds[ 'fifteens' ] );
$thirties         = explode( "\n", $flds[ 'thirties' ] );
$fifties          = explode( "\n", $flds[ 'fifties' ] );
$more             = explode( "\n", $flds[ 'more_than_fifty' ] );
@endphp

@component( 'comps.blocks', $componentVars )
<div class="squares-wrapper">
  <div class="donations-title">People who donated...</div>
  <div class="tabs donation-tabs">
    <ul class="tab-titles">
      <li class="active" data-id="more">More than &euro;50</li>
      <li data-id="fifty">&euro;50</li>
      <li data-id="thirty">&euro;30</li>
      <li data-id="fifteen">&euro;15</li>
      <li data-id="less">Less than &euro;15</li>
    </ul>
  </div>

  <div id="more" class="squares-wrap active">
    <div class="squares">
      @include('comps.blockbits.buy-a-brick', ['donations' => $more, 'acceptedDonations' => 100, 'figure' => ''])
    </div>
  </div>

  <div id="fifty" class="squares-wrap">
    <div class="squares">
      @include('comps.blockbits.buy-a-brick', ['donations' => $fifties, 'acceptedDonations' => 50, 'figure' => 50])
    </div>
  </div>

  <div id="thirty" class="squares-wrap">
    <div class="squares">
      @include('comps.blockbits.buy-a-brick', ['donations' => $thirties, 'acceptedDonations' => 150, 'figure' => 30])
    </div>
  </div>

  <div id="fifteen" class="squares-wrap">
    <div class="squares">
      @include('comps.blockbits.buy-a-brick', ['donations' => $fifteens, 'acceptedDonations' => 200, 'figure' => 15])
    </div>
  </div>

  <div id="less" class="squares-wrap">
    <div class="squares">
      @include('comps.blockbits.buy-a-brick', ['donations' => $less, 'acceptedDonations' => 100, 'figure' => ''])
    </div>
  </div>

</div>
@endcomponent
