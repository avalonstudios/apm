{{--
  Title: Donations
  Description: Donations block
  Category: ava_block_category
  Icon: admin-comments
  Keywords: donations, donate
  Mode: edit
  Align: full
  PostTypes: page post
  SupportsAlign: true
  SupportsMode: true
  SupportsMultiple: true
--}}

@php
$flds  = get_fields(  );
$active = $flds[ 'active' ];

if ( ! $active ) {
  return;
}

$image = $flds[ 'image' ][ 'url' ];
$image = aq_resize( $image, 960, 400, true, true, true );
$imageTitle = $flds[ 'image' ][ 'title' ];
$text = wp_kses_post( $flds[ 'text' ] );
$button = $flds[ 'button' ];

$other_classes = '';
$backImg = '';

$sectionTitle = $flds[ 'section_title' ];

$componentVars = [
  'id'              => $block[ 'id' ],
  'classes'         => $block[ 'classes' ],
  'slug'            => $block[ 'slug' ],
  'other_classes'   => " {$other_classes}",
  'title'           => '', // we're removing it coz we're putting it on the image
  'blockID'         => $block[ 'id' ],
  'secProps'        => $flds[ 'styles' ],
  'backImg'         => ''
];
@endphp

@component( 'comps.blocks', $componentVars )
  <div class="image" style="background-image: url( '{{ $image }}' );">
    @if ( $sectionTitle )
      <h2 class="section-title">{{ $sectionTitle }}</h2>
    @endif
    {{-- <img src="{{ $image }}" alt="{{ $imageTitle }}"> --}}
    <div class="underlay"></div>
  </div>
  <div class="block-content">
    <div class="text">{!! $text !!}</div>
    @include('comps.btns.btn')
  </div>

  <style>
    .image::after {
      border-color: transparent transparent @if ( $flds[ 'styles' ][ 'background_color' ] == '' ) white @else {{ $flds[ 'styles' ][ 'background_color' ] }} @endif transparent;
    }
  </style>
@endcomponent
