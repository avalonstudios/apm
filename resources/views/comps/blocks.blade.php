<section
  data-{{ $id }}
  id="{{ $blockID }}"
  class="ava-block {{ $classes }}{{ $other_classes }}"
  @if ( $backImg ) style="background-image: url( '{{ $backImg }}' );" @endif
  >

  <div class="ava-block-wrapper {{ $slug }}-wrapper">
    @if ( $title )
      <h2 class="section-title">{{ $title }}</h2>
    @endif
    {{ $slot }}
  </div>
  @include('partials.block-styles')
</section><!-- {{ $slug }} -->
