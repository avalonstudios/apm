@php
$opts               = App::getOptions();
$adoptionForm       = (int) $opts[ 'adoption_form' ];

$flds               = get_fields();
$main_image         = $flds[ 'main_image' ];
$adoption_code      = $flds[ 'adoption_code' ];
$desc               = $flds[ 'description' ];
$images             = $flds[ 'images' ];

$petName = get_the_title();
$petID = get_the_ID();
@endphp

<article @php post_class() @endphp>
  <div class="entry-content">

    @include('partials.pets.pet-features')

    @include('partials.pets.pet-images')

    {!! $desc !!}

    @if ( isset( $_GET[ 'adoption_code' ] ) )
      <footer>
        <div class="adoption-form">
          @php
          echo do_shortcode("[ninja_form id='{$adoptionForm}']")
          @endphp
        </div>
      </footer>
    @endif

  </div>

  @include('partials.posts-pagination')
</article>
