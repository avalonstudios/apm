@php
$ratio = round( 400 / 1.618 );
$img = get_the_post_thumbnail_url();
$img = aq_resize( $img, 400, $ratio, true, true, true );

$title = get_the_title();

$button[ 'url' ] = get_permalink();
$button[ 'title' ] = $title;
$button[ 'target' ] = '';
@endphp

<article @php post_class() @endphp>
  <div class="post-thumbnail" style="background-image:url('@if ($img) {{ $img }} @else @asset('images/repl-img.png') @endif');"></div>
  <header>
    <h2 class="entry-title"><a href="{{ get_permalink() }}">{!! $title !!}</a></h2>
    @include('partials/entry-meta')
  </header>
  <div class="entry-summary">
    @php the_excerpt() @endphp
  </div>
  <div class="btn-wrapper">
    @include('comps.btns.btn')
  </div>
</article>
