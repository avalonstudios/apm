@php
$textColor        = $secProps[ 'text_color' ];
$backgroundColor  = $secProps[ 'background_color' ];
$marginTop        = $secProps[ 'margin' ][ 'top' ];
$marginRight      = $secProps[ 'margin' ][ 'right' ];
$marginBottom     = $secProps[ 'margin' ][ 'bottom' ];
$marginLeft       = $secProps[ 'margin' ][ 'left' ];
$paddingTop       = $secProps[ 'padding' ][ 'top' ];
$paddingRight     = $secProps[ 'padding' ][ 'right' ];
$paddingBottom    = $secProps[ 'padding' ][ 'bottom' ];
$paddingLeft      = $secProps[ 'padding' ][ 'left' ];
@endphp

<style type="text/css">
  [data-{{ $id }}] {
    color: {{ $secProps[ 'text_color' ] }};
    background-color: {{ $secProps[ 'background_color' ] }};
    margin-top: {{ $secProps[ 'margin' ][ 'top' ] }};
    margin-right: {{ $secProps[ 'margin' ][ 'right' ] }};
    margin-bottom: {{ $secProps[ 'margin' ][ 'bottom' ] }};
    margin-left: {{ $secProps[ 'margin' ][ 'left' ] }};
    padding-top: {{ $secProps[ 'padding' ][ 'top' ] }};
    padding-right: {{ $secProps[ 'padding' ][ 'right' ] }};
    padding-bottom: {{ $secProps[ 'padding' ][ 'bottom' ] }};
    padding-left: {{ $secProps[ 'padding' ][ 'left' ] }};
  }
</style>
