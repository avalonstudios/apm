@php
if ( ! is_single() ) {
  return;
}
@endphp

<div class="post-navigation">
  @if ( get_previous_post_link() )
    <span class="prev-link">{!! previous_post_link() !!}</span>
  @endif

  @if ( get_next_post_link() )
    <span class="next-link">{!! next_post_link() !!}</span>
  @endif
</div>
