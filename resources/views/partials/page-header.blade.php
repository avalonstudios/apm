@php
$flds = get_fields();
$hasRedTitle = $flds[ 'red_title' ];

if ( is_front_page() ) {
  return;
} elseif ( is_shop() ) {
  $pageID = get_option( 'woocommerce_shop_page_id' );
  $img = get_the_post_thumbnail_url( $pageID, 'full' );
  $title = 'SHOP';
} elseif ( is_cart() ) {
  $pageID = get_option( 'woocommerce_cart_page_id' );
  $img = get_the_post_thumbnail_url( $pageID, 'full' );
  $title = App::title();
} elseif ( is_checkout() ) {
  $pageID = get_option( 'woocommerce_checkout_page_id' );
  $img = get_the_post_thumbnail_url( $pageID, 'full' );
  $title = App::title();
} elseif ( is_home() ) {
  $pageID = get_option( 'page_for_posts' );
  $img = get_the_post_thumbnail_url( $pageID, 'full' );
  $title = App::title();
} elseif ( is_singular( 'pet_cpt' ) ) {
  $img = get_field( 'main_image' );
  $title = App::title();
} elseif ( is_single() ) {
  $pageID = get_the_ID();
  $img = get_the_post_thumbnail_url( $pageID, 'full' );
  $title = App::title();
} elseif ( is_product() ) {
  $img = get_the_post_thumbnail_url();
  $title = get_the_title();
} else {
  $title = App::title();
}
if ( isset( $img ) ) {
  $img = aq_resize( $img, 1920, 335, true, true, true );
} else {
  $img = '';
}
@endphp

<div
  class="page-header{{ $img ? '' : ' no-image' }}{{ $hasRedTitle ? ' red-title' : '' }}"
  @if ( $img )
  style="background-image: url( '{{ $img }}' );"
  @endif
  >
  <h1>{{ $title }}</h1>
  @if ( is_product() )
  {!! woocommerce_breadcrumb() !!}
  @elseif ( is_single() )
  @include('partials.entry-meta')
  @endif
  <div class="underlay"></div>
</div>
