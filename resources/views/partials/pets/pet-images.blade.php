@if ( $images )
  <div class="pet-images">
    @foreach ( $images as $image )
      @php
      $img = $image[ 'image' ];
      $img = aq_resize( $img, 200, 200, true, true, true );
      @endphp
      <div class="image">
        <img src="{{ $img }}" alt="{{ $petName }} image {{ $loop->iteration }}" data-slide-id="{{ $loop->iteration }}">
      </div>
    @endforeach
  </div>
@endif

<div class="ava-pet-images-modal">
  <div class="pet-slider-wrapper">
    <div class="pet-modal-slider">
      @foreach ( $images as $image )
        @php
        $img = $image[ 'image' ];
        $img = aq_resize( $img, 1800, 900, false, true, true );
        @endphp
        <div class="image">
          <img src="{{ $img }}" alt="{{ $petName }} image {{ $loop->iteration }}" data-slide-id="{{ $loop->iteration }}">
        </div>
      @endforeach
    </div>
    <button class="close-slider btn btn-outline white">close</button>
  </div>
</div>
