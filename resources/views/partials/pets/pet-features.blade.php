@php
// $flds               = get_fields();
$main_image         = $flds[ 'main_image' ];
$adoption_code      = $flds[ 'adoption_code' ];
$gender             = $flds[ 'gender' ];
$breed              = $flds[ 'breed' ];
$age                = $flds[ 'age' ];
$age_unit           = $flds[ 'age_unit' ];
$color              = $flds[ 'color' ];
$previously_owned   = $flds[ 'previously_owned' ];
$entered_sanctuary  = $flds[ 'entered_sanctuary' ];
$images             = $flds[ 'images' ];
$desc               = $flds[ 'description' ];
@endphp

<div class="pet-features">
    @if ( $breed )
      @php
      $link = get_term_link( $breed, 'pet_category' );
      @endphp
      <div class="pet-feature breed">
        <span class="data-name">Breed</span>
        <span class="data-value"><a href="{{ $link }}" rel="bookmark">{{ $breed->name }}</a></span>
      </div>
    @endif

    @if ( $gender )
      @php
      $link = get_term_link( $gender, 'pet_gender' );
      @endphp
      <div class="pet-feature gender">
        <span class="data-name">Gender</span>
        <span class="data-value"><a href="{{ $link }}" rel="bookmark">{{ $gender->name }}</a></span>
      </div>
    @endif

    @if ( $age )
      <div class="pet-feature age">
        <span class="data-name">Age</span>
        <span class="data-value">{{ $age }} {{ $age_unit }}</span>
      </div>
    @endif

    @if ( $color )
      <div class="pet-feature color">
        <span class="data-name">Color</span>
        <span class="data-value">{{ $color }}</span>
      </div>
    @endif

    @if ( $previously_owned )
      @php
      $link = get_term_link( $previously_owned, 'pet_gender' );
      @endphp
      <div class="pet-feature previously_owned">
        <span class="data-name">Previously Owned</span>
        <span class="data-value"><a href="{{ $link }}" rel="bookmark">{{ $previously_owned->name }}</a></span>
      </div>
    @endif

    @if ( $entered_sanctuary )
      <div class="pet-feature entered_sanctuary">
        <span class="data-name">Entered Sanctuary</span>
        <span class="data-value">{{ date( 'jS M, Y', ( int ) $entered_sanctuary ) }}</span>
      </div>
    @endif
  </div>
