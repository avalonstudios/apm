@php
$flds               = get_fields();
$main_image         = $flds[ 'main_image' ];
$adoption_code      = $flds[ 'adoption_code' ];
// $pet_type           = $flds[ 'type' ];

// $petType = get_term( $pet_type, 'pet_category' );
// $categoryLink = get_term_link( $pet_type, 'pet_category' );

$ratio = round( 250 * 1.618 );
$img = aq_resize( $main_image, 250, $ratio, true, true, true );

$title = get_the_title();
$link = get_permalink();

$button[ 'url' ] = $link . '?adoption_code=' . $adoption_code;
$button[ 'title' ] = $title;
$button[ 'target' ] = '';
@endphp

<article @php post_class() @endphp>
  <div class="post-thumbnail">
    @if ($img)
      <img src="{{ $img }}" alt="{{ $title }}">
    @else
      <img src="@asset('images/repl-img.png')" alt="{{ $title }}">
    @endif
  </div>
  <header>
    <h2 class="entry-title"><a href="{{ $link }}" rel="bookmark">{!! $title !!}</a></h2>
  </header>
  @include('partials.pets.pet-features')
  <div class="btn-wrapper">
    @include('comps.btns.btn')
  </div>
</article>
