<header class="banner">
  <div class="container">
    <div class="ava-grid">
      <div class="site-logo">
        {!! App::getCustomLogo() !!}
        <a class="brand" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
      </div>
      @include('partials.menus.main-menu')
      @include('partials.menus.social-menu', [ 'place' => 'social-header' ])
      <div class="login-register-menu-large">
        @include( 'partials.menus.login-register-menu' )
      </div>
      <div class="login-register-menu-small">
        @include( 'partials.menus.login-register-menu' )
      </div>
    </div>
  </div>
</header>
