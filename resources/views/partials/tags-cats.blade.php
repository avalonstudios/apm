@php
$categories = wp_get_post_categories( get_the_ID() );
$tags = get_the_tags( get_the_ID() );
@endphp

@if ( $categories )
<div class="post-categories">
  <i class="fas fa-bookmark"></i>
  @foreach ( $categories as $c )
    @php
    $cat = get_category( $c );
    $link = get_term_link( $c );
    @endphp
    <a href="{{ $link }}">{{ $cat->name }}</a>@if ( ! $loop->last ), @endif
  @endforeach
</div>
@endif

@if ( $tags )
<div class="post-tags">
  <i class="fas fa-tag"></i>
  @foreach ( $tags as $c )
    @php
    $cat = get_category( $c );
    $link = get_term_link( $c );
    @endphp
    <a href="{{ $link }}">{{ $cat->name }}</a>@if ( ! $loop->last ), @endif
  @endforeach
</div>
@endif
