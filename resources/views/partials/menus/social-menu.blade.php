@php
$socialIcons = App::getOptions(  )[ 'social_icons' ][ 'social_icons' ];
@endphp

<div class="social-menu {{ $place }}">
  <ul class="social-icons">
      @each( 'comps.social-icon-comp', $socialIcons, 'sIcon' )
  </ul>
</div>
