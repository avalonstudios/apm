<div class="hamburger"><i class="fas fa-bars"></i></div>
<nav class="nav-primary">
  @if (has_nav_menu('primary_navigation'))
    {!! wp_nav_menu([
      'theme_location' => 'primary_navigation',
      'container_class' => 'main-menu',
      //'container_id' => '-container-id',
      //'menu_id' => 'ul-menu-id',
      'menu_class' => 'nav',
      'walker' => new MDBootstrapMenu(  )
      ])
    !!}
  @endif
</nav>
