<nav class="login-register-menu">
  <ul class="login-navigation list-unstyled">
    <li class="loginout-menu-item">
      {!! App::login_register_menu()[ 'loginout' ] !!}
    </li>
    <li class="register-menu-item">
      <a href="{!! App::login_register_menu()[ 'register' ] !!}">Register</a>
    </li>
    <li class="dg-cart-basket-icon">
      <a class="last-item" href="{{ App::login_register_menu()[ 'cart_url' ] }}">
        <i class="fas fa-shopping-cart"></i>
        <span class="cart-count">@php echo WC()->cart->get_cart_contents_count(); @endphp</span>
      </a>
      {{-- @include( 'shop.cart-popup' ) --}}
    </li>
  </ul>
</nav>
