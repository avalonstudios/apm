<article @php post_class() @endphp>
  <div class="entry-content">
    @php the_content() @endphp
  </div>
  <footer>
    {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
    @include('partials.tags-cats')
  </footer>
  @php comments_template('/partials/comments.blade.php') @endphp
  @include('partials.posts-pagination')
</article>
