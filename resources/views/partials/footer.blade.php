<footer class="content-info">
  <div class="container">
    <div class="footer-widgets">
      @php dynamic_sidebar('sidebar-footer') @endphp
    </div>
  </div>
  <div class="footer-credits">
    <span class="copyright">&copy; {{ get_bloginfo( 'name' ) }} {{ date( 'Y' ) }}</span>
    <span class="designer">Designed by <a href="https://avalonstudios.eu" rel="nofollow" target="_blank">Avalon Studios</a>.</span>
    <span class="powered">Powered by <a href="https://cloud1500.com" rel="nofollow" target="_blank">Cloud1500</a></span>
  </div>
</footer>
