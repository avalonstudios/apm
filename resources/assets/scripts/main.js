// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';
import singlePetCpt from './routes/single-pet_cpt';

// Import Slick
import 'slick-carousel/slick/slick.min';

// import then needed Font Awesome functionality
import { library, dom } from '@fortawesome/fontawesome-svg-core';
// import the Facebook and Twitter icons
import { faFacebookF, faTwitter, faLinkedinIn, faYoutube } from '@fortawesome/free-brands-svg-icons';
import { faPaw, faStar, faShoppingCart, faSortDown, faCaretRight, faCaretLeft, faBars, faTimes } from '@fortawesome/free-solid-svg-icons';

// add the imported icons to the library
library.add(faFacebookF, faTwitter, faLinkedinIn, faPaw, faStar, faShoppingCart, faSortDown, faCaretRight, faCaretLeft, faBars, faTimes, faYoutube);

// tell FontAwesome to watch the DOM and add the SVGs when it detects icon markup
dom.watch();

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
  // Single Pet
  singlePetCpt,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());
