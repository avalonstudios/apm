export default {
  init() {
    // JavaScript to be fired on all pages
  },
  finalize() {
    $( '.donation-tabs li' ).on( 'click', function( e ) {
      e.preventDefault();
      let $id = $( this ).data( 'id' );

      $( this ).addClass( 'active' );

      $( '.squares-wrap' ).removeClass( 'active' );
      $( '#' + $id ).addClass( 'active' );
    });

    $(window).on('click', () => {
      console.log('window')
    })

    $( '.nav-primary li.menu-item-has-children .drop-dropdown' ).click( function( e ) {
      e.preventDefault();

      var $this = $( this );

      if ( $this.next().hasClass( 'show' ) ) {
        $this.next().removeClass( 'show' );
        $this.next().slideUp( 350 );
      } else {
        $this.parent().parent().find( '.sub-menu' ).removeClass( 'show' );
        $this.parent().parent().find( '.sub-menu' ).slideUp( 350 );
        $this.next().toggleClass( 'show' );
        $this.next().slideToggle( 350) ;
      }
    });

    $( '.hamburger' ).on( 'click', () => {
      let $open  = $( '.hamburger, .nav-primary, .social-header, .login-register-menu-small' );

      if ( $open.hasClass( 'open' ) ) {
        $open.removeClass('open');
        $( '.hamburger svg' ).toggleClass( 'fa-bars fa-times' )
      } else {
        $open.addClass( 'open' );
        $( '.hamburger svg' ).toggleClass( 'fa-bars fa-times' )
      }
    });

    $('img').attr('loading', 'lazy');

    $( window ).on( 'resize', onResize );
  },
};

function onResize() {
  $( '.sub-menu' ).removeClass( 'show' ).css( 'display', '' );
}
