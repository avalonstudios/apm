export default {
  init() {
    // JavaScript to be fired on the home page
  },
  finalize() {

    /**
     * When user clicks on a gallery image
     */
    $('.pet-images img').on('click', function (e) {
      e.preventDefault();
      // grab the data-slide-id
      let $slideID = $(this).data('slide-id');
      // fade in modal with slick slides in it
      $('.ava-pet-images-modal').fadeIn();
      // slick the modal images
      galleryModalSlider($slideID);
    });

    $('.ava-pet-images-modal .close-slider').on('click', function () {
      shudownSlider();
    });

    $(document).keyup(function (e) {
      if (e.key === 'Escape') {
        shudownSlider();
      }
    });
  },
};

function shudownSlider() {
  $('.ava-pet-images-modal').fadeOut(function () {
    $('.pet-modal-slider').slick('unslick');
  });
}

function galleryModalSlider($slideID) {
  var settings = {
    dots: false,
    arrows: true,
    prevArrow: '<div class="custom-slick-arrow custom-slick-prev arrows-blue circled"><i class="fas fa-caret-left"></i></div>',
    nextArrow: '<div class="custom-slick-arrow custom-slick-next arrows-blue circled"><i class="fas fa-caret-right"></i></div>',
    infinite: true,
    fade: true,
    speed: 700,
    autoplay: false,
    autoplaySpeed: 2000,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  $('.pet-modal-slider').slick(settings);
  $('.pet-modal-slider').slick('slickGoTo', $slideID - 1);
}
